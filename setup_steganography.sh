npm install lwip@npm:@kant2002/lwip
git clone https://github.com/rodrigouroz/steganography.git
sed -i -e 's/0.0.9/1.0.0/g' steganography/package.json
npm install ./steganography

# steganography embed <host_file> <guest_file> [<output_file>] [<password>]
# steganography embed host.jpg secret.pdf output secret-password

# steganography dig-up <image_file> [<output_folder>] [<password>]
# steganography dig-up output.png ./ secret-password

